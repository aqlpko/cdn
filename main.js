
    const MAIN_URL = "http://192.168.43.121";
    const MANUAL_CONTROL_ADDRESS = "/manual";
    const SET_LIGHT_ADDRESS = "/light";
    const CANDLE_MODE_ADDRESS = "/candle";
	const TESTO_SITE = "https://testo.systems/";

    const redirectToSite = siteAddress => {
        window.location.href = siteAddress;
    };

    document.getElementById("setLightButton").onclick = () => redirectToSite(`${MAIN_URL}${SET_LIGHT_ADDRESS}`);
    document.getElementById("candleModeButton").onclick = () => redirectToSite(`${MAIN_URL}${CANDLE_MODE_ADDRESS}`);
    document.getElementById("manualControlButton").onclick = () => redirectToSite(`${MAIN_URL}${MANUAL_CONTROL_ADDRESS}`);
    document.getElementById("footer").onclick = () => redirectToSite(TESTO_SITE);
