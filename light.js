
    const MAIN_URL = "http://192.168.43.121";
    const MODULE_PAGE_ADDRESS = "/";
    const SET_LIGHT_ADDRESS = "/light";
		const TESTO_SITE = "https://testo.systems/";
    let selectedButton = "normalButton";

    let lightParameters = {
        "Timeline":[
        {
            "WhiteTemperature":4500,
            "WhitePower":10,
            "Time":0
        }
    ]};

    const redirectToSite = siteAddress => {
        window.location.href = siteAddress;
    };

    const requestLightChange = (lightTemperature, buttonId) => {
        if (lightTemperature != null)
        {
            lightParameters.Timeline[0].WhiteTemperature = lightTemperature;
        }
        const xhr = new XMLHttpRequest();
        xhr.open("POST", `${MAIN_URL}${SET_LIGHT_ADDRESS}`, true);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        xhr.onload = function(e) {
            if (buttonId != null && xhr.responseText == "OK")
            {
                const selectedButtonElement = document.getElementById(selectedButton);
                const buttonElement = document.getElementById(buttonId);
                selectedButtonElement.style.backgroundColor = '#ffffff';
                selectedButtonElement.style.color = '#464646';
                buttonElement.style.backgroundColor = '#464646';
                buttonElement.style.color = '#ffffff';
                selectedButton = buttonId;
            }
        }
        xhr.send(JSON.stringify(lightParameters));
    };

    const updateLightPowerValue = (slider) => document.getElementById("lightPowerLabel").textContent = `Light power: ${slider.value} %`;

    document.getElementById("home-page").onclick = () => redirectToSite(`${MAIN_URL}${MODULE_PAGE_ADDRESS}`);
    document.getElementById("footer").onclick = () => redirectToSite(TESTO_SITE);

    document.getElementById("veryColdWhiteButton").onclick = () => requestLightChange(6200, "veryColdWhiteButton");
    document.getElementById("coldWhiteButton").onclick = () => requestLightChange(5200, "coldWhiteButton");
    document.getElementById("normalButton").onclick = () => requestLightChange(4500, "normalButton");
    document.getElementById("warmWhiteButton").onclick = () => requestLightChange(3800, "warmWhiteButton");
    document.getElementById("veryWarmWhiteButton").onclick = () => requestLightChange(3200, "veryWarmWhiteButton");

    document.getElementById("lightPowerSlider").onchange = () => {
        lightParameters.Timeline[0].WhitePower = parseInt(document.getElementById("lightPowerSlider").value);
        requestLightChange();
    };

    document.getElementById("lightPowerSlider").oninput = function () {
        updateLightPowerValue(this);
    };
