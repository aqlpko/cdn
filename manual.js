    const MAIN_URL = "http://192.168.43.121";
    const MODULE_PAGE_ADDRESS = "/";
    const SET_LIGHT_ADDRESS = "/light";
    const TESTO_SITE = "https://testo.systems/";

    let lightParameters = {
        "Timeline":[
        {
            "ColorHexValue":"#000000",
            "ColorPower":100,
            "Time":0
        }
    ]};

    const redirectToSite = siteAddress => {
        window.location.href = siteAddress;
    };

    const formToJSON = elements => [].reduce.call(elements, (data, element) => {
        if (element.name !== "") {
            data[element.name] = element.value;
        }

        return data;
    }, {});

    const requestLightChange = () => {
        lightParameters.Timeline[0].ColorHexValue = `#${("0" + document.getElementById("redValue").value.toString(16)).slice(-2)}${("0" + document.getElementById("greenValue").value.toString(16)).slice(-2)}${("0" + document.getElementById("blueValue").value.toString(16)).slice(-2)}`;
        const xhr = new XMLHttpRequest();
        xhr.open("POST", `${MAIN_URL}${SET_LIGHT_ADDRESS}`, true);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        xhr.send(JSON.stringify(lightParameters));
        xhr.onload = function () {
            //updatePwmValuePanel(JSON.parse(xhr.response));
        };
    };

    const updateSliderLabelValue = (slider) => document.getElementById(`${slider.id}Label`).textContent = `${slider.id} PWM: ${slider.value} %`;

    document.getElementById("redValue").oninput = function () {
        updateSliderLabelValue(this);
    }

    document.getElementById("greenValue").oninput = function () {
        updateSliderLabelValue(this);
    }

    document.getElementById("blueValue").oninput = function () {
        updateSliderLabelValue(this);
    }

    document.getElementById("redValue").onchange = () => requestLightChange();
    document.getElementById("greenValue").onchange = () => requestLightChange();
    document.getElementById("blueValue").onchange = () => requestLightChange();
    document.getElementById("footer").onclick = () => redirectToSite(TESTO_SITE);
    document.getElementById("home-page").onclick = () => redirectToSite(`${MAIN_URL}${MODULE_PAGE_ADDRESS}`);