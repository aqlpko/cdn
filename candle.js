
    const MAIN_URL = "http://192.168.43.121";
    const MODULE_PAGE_ADDRESS = "/";
    const SET_LIGHT_ADDRESS = "/light";
    const TESTO_SITE = "https://testo.systems/";

    let lightParameters = {
        "Timeline":[
        {
            "ColorHexValue":"#FF0000",
            "ColorPower":10,
            "Time":10,
            "ColorChangeRate":-1
        },
        {
            "ColorHexValue":"#FF0000",
            "ColorPower":0,
            "Time":10,
            "ColorChangeRate":1
        }],
        Repeat: true
    };

    const redirectToSite = siteAddress => {
        window.location.href = siteAddress;
    };

    const updateSliderLabelValue = (slider) => {
        if (slider.id === "lightPowerSlider") {
            document.getElementById("lightPowerLabel").textContent = `Light power: ${slider.value} %`;
        } else {
            document.getElementById("frequencyLabel").textContent = `Frequency: ${slider.value} %`;
        }
    }

    const requestLightChange = () => {
        const xhr = new XMLHttpRequest();

        xhr.open("POST", `${MAIN_URL}${SET_LIGHT_ADDRESS}`, true);
        xhr.setRequestHeader('Content-Type', 'text/plain');
        xhr.send(JSON.stringify(lightParameters));
    };

    const updateChangeRate = () => {
        lightParameters.Timeline[0].ColorChangeRate = -1 * lightParameters.Timeline[0].ColorPower / (lightParameters.Timeline[0].Time);
        lightParameters.Timeline[1].ColorChangeRate = lightParameters.Timeline[0].ColorPower / (lightParameters.Timeline[0].Time);
    }

    document.getElementById("home-page").onclick = () => redirectToSite(`${MAIN_URL}${MODULE_PAGE_ADDRESS}`);
    document.getElementById("footer").onclick = () => redirectToSite(TESTO_SITE);

    document.getElementById("lightPowerSlider").oninput = function () {
        updateSliderLabelValue(this);
    }

    document.getElementById("frequencySlider").oninput = function () {
        updateSliderLabelValue(this);
    }

    document.getElementById("lightPowerSlider").onchange = () => {
        const power = parseInt(document.getElementById("lightPowerSlider").value);
        lightParameters.Timeline[0].ColorPower = power;
        requestLightChange();
    }

    document.getElementById("frequencySlider").onchange = () => {
        const frequency = parseInt(document.getElementById("frequencySlider").value);

        if (frequency == 0) {
            lightParameters.Timeline[0].Time = 0;
        }
        else {
            lightParameters.Timeline[0].Time = 101 - frequency;
            lightParameters.Timeline[1].Time = 101 - frequency;
        }

        requestLightChange();
    }

    document.getElementById("colorPicker").onchange = () => {
        const hex = document.getElementById("colorPicker").value.toString();
        lightParameters.Timeline[0].ColorHexValue = hex;
        lightParameters.Timeline[1].ColorHexValue = hex;
        requestLightChange();
    }
